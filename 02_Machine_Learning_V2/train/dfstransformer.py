from featuretools.computational_backends import calculate_feature_matrix
from featuretools.synthesis import dfs
import featuretools as ft
from sklearn.base import TransformerMixin
import pandas as pd

class DFSTransformer(TransformerMixin):
    """Transformer using Scikit-Learn interface for Pipeline uses.
    """
    def __init__(self,
                 columns=None,
                 target_entity=None,
                 agg_primitives=None,
                 trans_primitives=None,
                 allowed_paths=None,
                 max_depth=2,
                 ignore_entities=None,
                 ignore_variables=None,
                 seed_features=None,
                 drop_contains=None,
                 drop_exact=None,
                 where_primitives=None,
                 max_features=-1,
                 verbose=False):
        self.es = ft.EntitySet(id='diabetes')
        self.feature_defs = []
        self.target_entity = target_entity
        self.agg_primitives = agg_primitives
        self.trans_primitives = trans_primitives
        self.allowed_paths = allowed_paths
        self.max_depth = max_depth
        self.ignore_entities = ignore_entities
        self.ignore_variables = ignore_variables
        self.seed_features = seed_features
        self.drop_contains = drop_contains
        self.drop_exact = drop_exact
        self.where_primitives = where_primitives
        self.max_features = max_features
        self.verbose = verbose
        self.columns = columns


    def fit(self, X, y=None):
        X = pd.DataFrame(data=X, columns=self.columns)
        self.es.entity_from_dataframe(entity_id='data', dataframe=X, index='index')
        es, entities, relationships, _ = self._parse_x_input(self.es)
        self.es = es

        self.feature_defs = dfs(entityset=es,
                                entities=entities,
                                relationships=relationships,
                                target_entity=self.target_entity,
                                agg_primitives=self.agg_primitives,
                                trans_primitives=self.trans_primitives,
                                allowed_paths=self.allowed_paths,
                                max_depth=self.max_depth,
                                ignore_entities=self.ignore_entities,
                                ignore_variables=self.ignore_variables,
                                seed_features=self.seed_features,
                                drop_contains=self.drop_contains,
                                drop_exact=self.drop_exact,
                                where_primitives=self.where_primitives,
                                max_features=self.max_features,
                                features_only=True,
                                verbose=self.verbose)

        return self


    def transform(self, X):
        X = pd.DataFrame(data=X,columns=self.columns)
        self.es.entity_from_dataframe(entity_id='data', dataframe=X, index='index')
        es, entities, relationships, cutoff_time = self._parse_x_input(self.es)
        self.es = es

        X_transformed = calculate_feature_matrix(
            features=self.feature_defs,
            instance_ids=None,
            cutoff_time=cutoff_time,
            entityset=es,
            entities=entities,
            relationships=relationships,
            verbose=self.verbose)

        return X_transformed


    def _parse_x_input(self, X):
        if isinstance(X, tuple):
            if isinstance(X[0], tuple):
                # Input of ((entities, relationships), cutoff_time)
                entities = X[0][0]
                relationships = X[0][1]
                es = None
                cutoff_time = X[1]
            elif isinstance(X[0], dict):
                # Input of (entities, relationships)
                entities = X[0]
                relationships = X[1]
                es = None
                cutoff_time = None
            else:
                # Input of (entityset, cutoff_time)
                es = X[0]
                entities = None
                relationships = None
                cutoff_time = X[1]
        else:
            # Input of entityset
            es = X
            entities = None
            relationships = None
            cutoff_time = None

        return es, entities, relationships, cutoff_time
