from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.metrics import plot_precision_recall_curve
import matplotlib.pyplot as plt

def plot_multi_class_precision_recall_curve(automl_pipeline, X_test, y_test, classes):
    """
    automl_pipeline: Your ML pipeline
    X_test: feature engineered features
    y_test: target labels
    classes: list of class names
    """
    y_test_bin = label_binarize(y_test, classes=classes) # change the classes
    n_classes = y_test_bin.shape[1]
    try:
    y_score = automl_pipeline.decision_function(X_test) # change X_test to your fitted_x_test
    except Exception as e:
    y_score = automl_pipeline.predict_proba(X_test) # change X_test to your fitted_x_test


    # For each class

    precision = dict()
    recall = dict()
    average_precision = dict()
    for i in range(n_classes):
      precision[i], recall[i], _ = precision_recall_curve(y_test_bin[:, i],
                                                          y_score[:, i])
      average_precision[i] = average_precision_score(y_test_bin[:, i], y_score[:, i])

    # A "micro-average": quantifying score on all classes jointly
    precision["micro"], recall["micro"], _ = precision_recall_curve(y_test_bin.ravel(),
      y_score.ravel())
    average_precision["micro"] = average_precision_score(y_test_bin, y_score,
                                                      average="micro")
    print('Average precision score, micro-averaged over all classes: {0:0.2f}'
        .format(average_precision["micro"]))



    plt.figure()
    plt.step(recall['micro'], precision['micro'], where='post')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title(
      'Average precision score, micro-averaged over all classes: AP={0:0.2f}'
      .format(average_precision["micro"]))


def plot_multi_class_roc_auc_curve(automl_pipeline, X_test, y_test, classes):
    """
    automl_pipeline: Your ML pipeline
    X_test: feature engineered features
    y_test: target labels
    classes: list of class names
    """
    y_test_bin = label_binarize(y_test, classes=[1,2,3,4])
    n_classes = y_test_bin.shape[1]
    try:
    y_score = automl_pipeline.decision_function(X_test)
    except Exception as e:
    y_score = automl_pipeline.predict_proba(X_test)

    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
      fpr[i], tpr[i], _ = roc_curve(y_test_bin[:, i], y_score[:, i])
      roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test_bin.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    # Plot of a ROC curve for a specific class
    plt.figure()
    plt.plot(fpr[2], tpr[2], label='ROC curve (area = %0.2f)' % roc_auc[2])
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()

    # Plot ROC curve
    plt.figure()
    plt.plot(fpr["micro"], tpr["micro"],
          label='micro-average ROC curve (area = {0:0.2f})'
                ''.format(roc_auc["micro"]))
    for i in range(n_classes):
      plt.plot(fpr[i], tpr[i], label='ROC curve of class {0} (area = {1:0.2f})'
                                    ''.format(i, roc_auc[i]))

    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Some extension of Receiver operating characteristic to multi-class')
    plt.legend(loc="lower right")
    plt.show()