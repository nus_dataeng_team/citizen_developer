## Launch Server

Docker Compose
```bash
# Build and run api + redis
docker-compose up -d --build

# Run api + redis 
docker-compose up -d

# Build and run api only
docker-compose up -d --build api
```

API only (old)
```bash
docker build -t myapi .
docker run -d --name myapicontainer -p80:80 myapi
```

## Populate model cache

```bash
python model_cache.py
```

## Client App
`tool.py` is provided as a sample client app.

Python package reqirements: requests

## Directory Structure for Server app

```
.
├── app
│   ├── model.py
│   └── main.py
├── models
│   ├── feature_defs.joblib.bz2
│   ├── fitted_pca.joblib.bz2
│   ├── scaler.joblib.bz2
│   └── tpot_pipeline.joblib.bz2
└── dockerfile
```


## On Launch
On launch, docker will run the following command
`uvicorn app.main:app --host 0.0.0.0 --port 80`