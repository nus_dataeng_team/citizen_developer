from pydantic import BaseModel

class ModelParams(BaseModel):
    # ['preg', 'glu', 'pres', 'skin', 'insulin', 'bmi', 'func', 'age']
    # [[  2.  , 112.  ,  75.  ,  32.  ,   0.  ,  35.7 ,   0.15,  21.  ]]
    preg: float
    glu: float
    pres: float
    skin: float
    insulin: float
    bmi: float
    func: float
    age: float

class PredictionResult(BaseModel):
    prediction: int
