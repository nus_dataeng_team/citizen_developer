import joblib
import redis
import zlib
import pickle
import xxhash

DEFAULT_COMPRESSION='gzip'
DEFAULT_TTL = 86400

def obj_serialize(obj,compression="gzip"):
    if obj is None:
        return None
    if compression == "gzip":
        ser_obj = zlib.compress(pickle.dumps(obj))
    elif compression is None or compression == "":
        # no compression
        ser_obj = pickle.dumps(obj)
    else:
        # unknown compression specified
        print(f"obj_ser: unknown compression {compression}")
        ser_obj = obj
    return ser_obj

def obj_deserialize(obj,compression="gzip"):
    if obj is None:
        return None
    if compression == "gzip":
        deser_obj = pickle.loads(zlib.decompress(obj))

    elif compression is None or compression == "":
        # no compression
        deser_obj = pickle.loads(obj)
    else:
        # unknown compression specified
        print(f"obj_deser: unknown compression {compression}")
        deser_obj = obj
    return(deser_obj)


class RedisCache():
    def __init__(self):
        self.conn = redis.Redis(host='redis', port=6379)

    def set_obj(self,obj,key,ttl=DEFAULT_TTL,namespace=None,
                compression=DEFAULT_COMPRESSION,pipeline=False):


        if namespace is not None:
            key = f"{namespace}:{key}"
        serialized_obj = obj_serialize(obj,compression=compression)

        if ttl == -1:
            self.conn.set(key,serialized_obj)
        else:
            self.conn.setex(key,ttl,serialized_obj)

        # digest
        digest_key = f"{key}_digest"
        digest = xxhash.xxh3_64_hexdigest(serialized_obj)
        if ttl == -1:
            self.conn.set(digest_key,digest)
        else:
            self.conn.setex(digest_key,ttl,digest)
        return digest

    def get_obj(self,key,namespace=None,
                compression=DEFAULT_COMPRESSION,pipeline=False):
        # check invalid key
        if not isinstance(key,str):
            return None

        if namespace is not None:
            key = f"{namespace}:{key}"
        if self.conn.exists(key):

            val = self.conn.get(key)
            obj = obj_deserialize(val,compression=compression)
            return(obj)
        else:
            # key does not exist
            return None



def populate():
    try:
        automl_pipeline = joblib.load("/model/v2/tpot_pipeline.joblib.bz2")
        data_pipeline = joblib.load("/model/v2/feat_eng_pipeline.joblib.bz2")

        cache = RedisCache()

        cache.set_obj(automl_pipeline, 'automl_pipeline', namespace='model')
        cache.set_obj(data_pipeline, 'data_pipeline', namespace='model')


        return True
    except Exception as e:
        return False

if __name__ == '__main__':
    populate()

