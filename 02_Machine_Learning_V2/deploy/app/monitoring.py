import os
from typing import Callable

import numpy as np
from prometheus_client import Counter, Histogram
from prometheus_fastapi_instrumentator import Instrumentator, metrics
from prometheus_fastapi_instrumentator.metrics import Info

NAMESPACE = os.environ.get("METRICS_NAMESPACE", "fastapi")
SUBSYSTEM = os.environ.get("METRICS_SUBSYSTEM", "model")

instrumentator = Instrumentator()

# ----- custom metrics -----
def classification_model_output(
    metric_name: str = "classification_model_output",
    metric_doc: str = "Output value of classification model",
    buckets = (0,1 )
) -> Callable[[Info], None]:
    METRIC = Histogram(
        metric_name,
        metric_doc,
        #labelnames=("diabetes_status",),
        buckets = buckets,
        namespace="fastapi",
        subsystem="model",
    )

    def instrumentation(info: Info) -> None:
        if info.modified_handler == "/predict":
            predicted = info.response.headers.get("X-model-score")
            if predicted:
                METRIC.observe(int(predicted))

    return instrumentation

# ----- add metrics -----
instrumentator.add(
    metrics.request_size(
        should_include_handler=True,
        should_include_method=True,
        should_include_status=True,
        metric_namespace=NAMESPACE,
        metric_subsystem=SUBSYSTEM,
    )
)
instrumentator.add(
    metrics.response_size(
        should_include_handler=True,
        should_include_method=True,
        should_include_status=True,
        metric_namespace=NAMESPACE,
        metric_subsystem=SUBSYSTEM,
    )
)
instrumentator.add(
    metrics.latency(
        should_include_handler=True,
        should_include_method=True,
        should_include_status=True,
        metric_namespace=NAMESPACE,
        metric_subsystem=SUBSYSTEM,
    )
)
instrumentator.add(
    metrics.requests(
        should_include_handler=True,
        should_include_method=True,
        should_include_status=True,
        metric_namespace=NAMESPACE,
        metric_subsystem=SUBSYSTEM,
    )
)

instrumentator.add(classification_model_output())
