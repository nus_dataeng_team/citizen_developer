import featuretools as ft
import joblib
import pandas as pd
from model_cache import RedisCache
import model_cache

class ClassificationModel():

  def __init__(self):

    self.cache = RedisCache()

    self.automl_pipeline = self.cache.get_obj('automl_pipeline', namespace='model')
    self.data_pipeline = self.cache.get_obj('data_pipeline', namespace='model')


    print("Loaded from cache")
    self.cols = ['preg', 'glu', 'pres', 'skin', 'insulin', 'bmi', 'func', 'age']

    # Fallback
    if not self.automl_pipeline:
      self.automl_pipeline = joblib.load("/model/v2/tpot_pipeline.joblib.bz2")
      print("Loaded from file.")
    if not self.data_pipeline:
      self.data_pipeline = joblib.load("/model/v2/feat_eng_pipeline.joblib.bz2")
      print("Loaded from file.")


  def update_model(self):
    ret = model_cache.populate()
    self.automl_pipeline = self.cache.get_obj('automl_pipeline', namespace='model')
    self.data_pipeline = self.cache.get_obj('data_pipeline', namespace='model')
    return ret


  def transform(self, X_test):
    # Scaling
    X_test = self.data_pipeline.transform(X_test)

    return X_test

  def predict(self, preg, glu,
            pres, skin,
            insulin, bmi,
            func, age):
    x_test = [[preg, glu,
            pres, skin,
            insulin, bmi,
            func, age]]
    transformed_data = self.transform(x_test)
    y_pred = self.automl_pipeline.predict(transformed_data)
    return y_pred[0]
