from typing import Callable
from fastapi import FastAPI, Response

from model import ClassificationModel
import redis
from monitoring import instrumentator
from schema import ModelParams, PredictionResult


app = FastAPI()
instrumentator.instrument(app).expose(app, include_in_schema=False)
cm = ClassificationModel()
cache = redis.Redis(host='redis', port=6379)




@app.post("/predict", response_model = PredictionResult)
def predict(response: Response, params: ModelParams):
    cache.incr('hits')
    pred = cm.predict(
            params.preg, params.glu,
            params.pres, params.skin,
            params.insulin, params.bmi,
            params.func, params.age
            )
    response.headers["X-model-score"] = str(pred)
    return PredictionResult(prediction = int(pred))


@app.get("/update_model")
def update_model():
    ret = cm.update_model()
    return {'updated': ret}


@app.get("/healthcheck")
async def healthcheck():
    return {"staus": "ok"}
