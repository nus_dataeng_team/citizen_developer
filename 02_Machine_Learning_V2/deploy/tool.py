import requests

def main():
    #payload = pd.DataFrame([[  2.  , 112.  ,  75.  ,  32.  ,   0.  ,  35.7 ,   0.15,  21.  ]]) # first entry in X_test
    # 9,171,110,24,240,45.4,0.721,54,1
    # 103,66,32,0,39.1,0.344,31
    url = 'http://localhost/predict'
    # data = {
    #     'preg':9., 'glu': 171., 'pres':110., 'skin':24., 'insulin':240.,
    #     'bmi':45.4, 'func':0.72, 'age':54.}
    data = {
        'preg':2., 'glu':112., 'pres':75., 'skin':24., 'insulin':0.,
        'bmi':35.4, 'func':0.15, 'age':21.}
    print("Sending: ")
    print(data)

    print("\nReceived:")
    resp = requests.post(url, json=data)
    print(resp.headers)
    print(resp.json())

if __name__ =='__main__':
    main()
