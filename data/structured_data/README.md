# Structured Datasets

- `students*.csv` datasets are modified versions of the following dataset.

```
P. Cortez and A. Silva. Using Data Mining to Predict Secondary School Student Performance. In A. Brito and J. Teixeira Eds., Proceedings of 5th FUture BUsiness TEChnology Conference (FUBUTEC 2008) pp. 5-12, Porto, Portugal, April, 2008, EUROSIS, ISBN 978-9077381-39-7.
Available at: ![Web Link](http://www3.dsi.uminho.pt/pcortez/student.pdf)
```

- `sample_datav3.json` is a handcrafted dataset based on CISCO PRIME logs


- `datasets_436_920_xAPI-Edu-Data.csv`

```
Amrieh, E. A., Hamtini, T., & Aljarah, I. (2016). Mining Educational Data to Predict Student’s academic Performance using Ensemble Methods. International Journal of Database Theory and Application, 9(8), 119-136.

Amrieh, E. A., Hamtini, T., & Aljarah, I. (2015, November). Preprocessing and analyzing educational data set using X-API for improving student's performance. In Applied Electrical Engineering and Computing Technologies (AEECT), 2015 IEEE Jordan Conference on (pp. 1-5). IEEE.
```

- `insurace.csv`
Available at ![Link](https://github.com/stedy/Machine-Learning-with-R-datasets)
