from flask import Flask
from flask_cors import CORS
from flask import request, jsonify

import json
import sys
import time

import pandas as pd

from student_classifier import StudentClassifier

PORT=13333

app = Flask(__name__)
CORS(app)

@app.route('/classify', methods=['POST'])
def classify_route():
    """
    Classification route
    Parameters:
    data point
    """
    if request.method == 'POST':
        status = 200
                
        # Payload load as dataframe
        payload = request.get_json()
        payload = pd.DataFrame(payload)

        # Run inference with trained model
        try:
            result = app.config['stuclf'].classify(payload)
            status = 200
        except Exception as e:
            print("[!] classify error")
            print(e)
            status = 500

        # Return data
        ret = {
            'prediction': result,
            'status': status
        }
        
        return jsonify(ret)
        
    elif request.method == 'GET':
        return jsonify({'error': 'Invalid Method.', 'status': 404})
                
# ROUTE ENDS
def init():
    stuclf = StudentClassifier()
    if not stuclf:
        print('[!] Error loading model')
        sys.exit()
        
    app.config.update({
        'stuclf': stuclf,
    }
    )
    
            
if __name__ == "__main__":
    init()
    app.run(host='0.0.0.0', port=PORT)
                                                
