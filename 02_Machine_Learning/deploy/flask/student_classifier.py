from joblib import load, dump
import numpy as np
import pandas as pd
from sklearn.preprocessing import Normalizer, LabelEncoder

# load model
class StudentClassifier():
    def __init__(self):
        """
        Load pre-trained: classification model, preprocessing pipeline, label encoder
        """
        self.clf = load('models/exported_modelv2.joblib')
        self.preproc = load('models/preprocv2.joblib')
        self.lbl_enc = load('models/label_encoderv2.joblib')
        
    def classify(self, df):
        """
        Classification function
        """
        try:
            del df['id']
        except Exception as e:
            pass

        # Preprocessing on features
        df = self.preproc.transform(df)

        # Resolve results into passed: yes/no
        res = self.lbl_enc.inverse_transform(self.clf.predict(df))

        return res[0]
    
    def test(self, df_test, df_test_labels):
        # run test based on metric
        print(df_test)

        # Preprocess features
        df_test = self.preproc.transform(df_test)

        # Test model
        return self.clf.score( df_test, df_test_labels)



def main():
    # Define dataset path
    DATASET='../../01_python_for_ds/student_data.parquet'

    # Load label encoder
    lbl_encoder = load('models/label_encoderv2.joblib')
    
    # Load dataset
    df = pd.read_parquet(DATASET)

    # REMOVE ID COLUMN and label column
    del df['id']

    # ONE HOT ENCODING of categorical variables
    df_label = lbl_encoder.transform(df['passed'])
    del df['passed']

    # Classifier 
    
    stuclf = StudentClassifier()

    score = stuclf.test(df, df_label)
    print("Test score: %f" % score)

    res = stuclf.classify(df)
    print(lbl_encoder.inverse_transform(res))
    
if __name__ == '__main__':
    main()



