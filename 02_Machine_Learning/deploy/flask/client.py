import requests
import json

# Open sample data point that we want to run inference on
with open('single_sample.json', 'r') as f:
    payload = json.load(f)
    
    # Preview data
    print('[!] Preview data:')
    print(payload)

print("POST to API server")
r = requests.post('http://127.0.0.1:13333/classify', json = payload)
print("Request status code: %d" % r.status_code)
print(r.json())
