import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import Normalizer, LabelEncoder
from tpot.builtins import StackingEstimator
from tpot.export_utils import set_param_recursive
from sklearn.preprocessing import FunctionTransformer
from copy import copy
from joblib import dump, load


# Define dataset path
DATASET='../../01_python_for_ds/student_data.parquet'

# Load dataset
df = pd.read_parquet(DATASET)

# make a copy of the label
df_labels = df['passed'].copy()

# REMOVE ID COLUMN and label column
del df['id']
del df['passed']


# ONE HOT ENCODING of categorical variables
df = pd.get_dummies(df)
df['passed'] = df_labels.copy()

# Split dataset into trainset and testset
df_train, df_test = train_test_split(
    df,
    train_size = 0.75,
    shuffle=True,
    random_state=25,
    stratify= df['passed'], # balance train/test set with labels

)


# Label encoder for train and test targets
# Fit on train set
label_encoder = LabelEncoder().fit(df_train['passed'])

# transform train and test sets labels
# Used only for development phase
df_train_labels = label_encoder.transform(df_train['passed'])
df_test_labels = label_encoder.transform(df_test['passed'])

# Remove label column from feature sets
del df_train['passed']
del df_test['passed']


# Average CV score on the training set was: 0.8740217061305288
exported_pipeline = make_pipeline(
    make_union(
        FunctionTransformer(copy),
        FunctionTransformer(copy)
    ),
    Normalizer(norm="l1"),
    RandomForestClassifier(bootstrap=True, criterion="gini", max_features=0.6000000000000001, min_samples_leaf=17, min_samples_split=13, n_estimators=100)
)

# Fix random state for all the steps in exported pipeline
set_param_recursive(exported_pipeline.steps, 'random_state', 42)

exported_pipeline.fit(df_train, df_train_labels)
results = exported_pipeline.predict(df_test)
print("Test score: ")
print(exported_pipeline.score( df_test, df_test_labels))

dump(exported_pipeline, 'exported_model.joblib')

# test if save model works

# load model
clf = load('exported_model.joblib')

# run test based on metric
print("Test score: ")
print(clf.score( df_test, df_test_labels))
