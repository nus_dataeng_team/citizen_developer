import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import Normalizer, LabelEncoder, OneHotEncoder
from tpot.builtins import StackingEstimator
from tpot.export_utils import set_param_recursive
from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from copy import copy
from joblib import dump, load


# Define dataset path
DATASET='../../01_python_for_ds/student_data.parquet'

# Load dataset
df = pd.read_parquet(DATASET)

# NOTE: Make sure that the outcome column is labeled 'target' in the data file
# REMOVE ID COLUMN and label column

del df['id']


categorical_cols = [
    'school', 'sex', 'address', 'famsize',
    'Pstatus', 'Mjob', 'Fjob', 'reason', 'guardian',
    'schoolsup', 'famsup', 'paid', 'activities', 'nursery',
    'higher', 'internet', 'romantic', ]

numeric_cols = [
        'Dalc', 'Fedu', 'Medu', 'Walc', 'absences',
        'age', 'failures', 'famrel', 'freetime', 'goout', 'health',
        'studytime', 'traveltime'
        ]


# Simple filling up of missing values for numeric columns
numeric_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='median')),
])

# ONE HOT ENCODING of categorical variables
categorical_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
    ('onehot', OneHotEncoder(handle_unknown='ignore'))
])

# Fi
lbl_transformer = Pipeline(
    steps=[
        ('imputer', SimpleImputer(strategy='constant', fill_value='missing'))
    ]
)

preprocessor = ColumnTransformer(
    transformers=[
        ('num', numeric_transformer, numeric_cols),
        ('cat', categorical_transformer, categorical_cols),
    ],
)

preproc_pipeline = Pipeline(
    steps=[
        ('preprocessor', preprocessor)
    ]
)

# Split dataset into trainset and testset
df_train, df_test = train_test_split(
        df,
        train_size = 0.75,
        shuffle=True,
        random_state=25,
        stratify= df['passed'], # balance train/test set with labels

    )


# Label encoder for train and test targets
# Fit on train set
df_train_labels = df_train['passed'].copy()
df_test_labels = df_test['passed'].copy()

label_encoder = LabelEncoder().fit(df_train_labels)

# transform train and test sets
df_train_labels = label_encoder.transform(df_train_labels)
df_test_labels = label_encoder.transform(df_test_labels)

# Remove label column from feature sets
del df_train['passed']
del df_test['passed']

# Transform features

preproc = preproc_pipeline.fit(df_train)
df_train = preproc.transform(df_train)
df_test = preproc.transform(df_test)


# Save preprocessors
dump(label_encoder, 'label_encoder.joblib')
dump(preproc, 'preproc.joblib')


# Average CV score on the training set was: 0.872114873291761
exported_pipeline = make_pipeline(
    make_union(
        FunctionTransformer(copy),
        FunctionTransformer(copy)
    ),
    Normalizer(norm="l1"),
    RandomForestClassifier(bootstrap=True, criterion="gini", max_features=0.9500000000000001, min_samples_leaf=18, min_samples_split=13, n_estimators=100)
)

# Fix random state for all the steps in exported pipeline
set_param_recursive(exported_pipeline.steps, 'random_state', 42)

exported_pipeline.fit(df_train, df_train_labels)

print("Test score: ")
print(exported_pipeline.score( df_test, df_test_labels))

dump(exported_pipeline, 'exported_model.joblib')

# test if save model works

# load model
clf = load('exported_model.joblib')

# run test based on metric
print("Test score: ")
print(clf.score( df_test, df_test_labels))
