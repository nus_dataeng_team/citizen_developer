from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
import pandas as pd
import os
import numpy as np
from joblib import dump, load

########
# DATA #
########

# Define dataset path
DATASET='../../01_python_for_ds/student_data.parquet'

# Load dataset
df = pd.read_parquet(DATASET)

# REMOVE ID COLUMN and label column
del df['id']

# Columns
categorical_cols = ['school', 'sex', 'address', 'famsize',
                    'Pstatus', 'Mjob', 'Fjob', 'reason', 'guardian',
                    'schoolsup', 'famsup', 'paid', 'activities', 'nursery',
                    'higher', 'internet', 'romantic', ]
numeric_cols = [
    'Dalc', 'Fedu', 'Medu', 'Walc', 'absences',
    'age', 'failures', 'famrel', 'freetime', 'goout', 'health',
    'studytime', 'traveltime'
    ]

######################
# Data Preprocessing #
######################

# Simple filling up of missing values for numeric columns
numeric_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='median')),
    ])

# ONE HOT ENCODING of categorical variables
categorical_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
        ('onehot', OneHotEncoder(handle_unknown='ignore'))])


preprocessor = ColumnTransformer(
    transformers=[
        ('num', numeric_transformer, numeric_cols),
        ('cat', categorical_transformer, categorical_cols),
    ],
    remainder='passthrough'
)

preproc_pipeline = Pipeline(
    steps=[
        ('preprocessor', preprocessor)
    ]
)


# Split dataset into trainset and testset
df_train, df_test = train_test_split(
    df,
    train_size = 0.75,
    shuffle=True,
    random_state=25,
    stratify= df['passed'], # balance train/test set with labels

)

# Fit data into preprocessing pipeline for it to 'learn' how to preprocess
preproc = preproc_pipeline.fit(df_train)

# Transform data with preprocessing steps
df_train = preproc.transform(df_train)
df_test = preproc.transform(df_test)


# Label encoder for train and test targets
# Fit on train set only
label_encoder = LabelEncoder().fit(df_train[:,-1])

# transform train and test sets
df_train_labels = label_encoder.transform(df_train[:,-1])
df_test_labels = label_encoder.transform(df_test[:,-1])


# Remove label column from feature sets
df_train = np.delete(df_train, -1, axis=1)
df_test = np.delete(df_test, -1, axis=1)

# Save preprocessors so we can use them during deployment
# Fitting preprocessors will create a map between strings found in features to its numerical form.
# This mapping has to be consistent when running both training and inference.
# So we need to save the mapping created by the transformations in the preprocessing pipeline.
dump(label_encoder, 'label_encoder.joblib')
dump(preproc, 'preproc.joblib')


################
# Model Search #
################

# Define our TPOT Classifier settings
pipeline_optimizer = TPOTClassifier(
    generations=50,
    population_size=60, cv=6,
    warm_start=False,
    scoring='average_precision',
    random_state=42, verbosity=2)


# Train the classifier
print("Training: ")
pipeline_optimizer.fit(df_train.astype(np.float64), df_train_labels.astype(np.uint8))

# Metrics
print("Test score: ")
print(pipeline_optimizer.score(df_test.astype(np.float64), df_test_labels.astype(np.uint8)))

# EXPORT
pipeline_optimizer.export('tpot_exported_pipeline.py')
